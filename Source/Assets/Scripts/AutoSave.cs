﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public struct Save
{
    public bool male;
    public int skin;
}

[System.Serializable]
public struct SphereSave
{
    public Vector3 position;
    public bool left;
    public bool right;
    public int order;
}

[System.Serializable]
public struct SphereExSave
{
    public List<SphereSave> ex;
}

[System.Serializable]
public struct ModelSave
{
    public Animation anim;
    public int order;
}

[System.Serializable]
public struct ModelExSave
{
    public List<ModelSave> ex;
}

public class AutoSave : MonoBehaviour
{
    public Save MainSave;
    public List<GameObject> models;
    public List<SphereExSave> sphereSaves;

    public GameObject prefab_sphere;

    // Start is called before the first frame update
    void Start()
    {
        MainSave.male = true;
        MainSave.skin = 0;

        if (!Directory.Exists(Application.dataPath + "/MainSave"))
        {
            Directory.CreateDirectory(Application.dataPath + "/MainSave");
        }

        if (!File.Exists(Application.dataPath + "/MainSave" + "/" + "Save" + ".json"))
        {
            save();
        }
        else
        {
            //MainSave = JsonUtility.FromJson<Save>(LoadJsonFromLocation(Application.dataPath + "/MainSave" + "/" + "Save" + ".json"));
            /*foreach(GameObject g in models)
            {
                if (g.gameObject.GetComponent<MainPlayerDataInput>() != null)
                {
                    g.gameObject.GetComponent<MainPlayerDataInput>().male = MainSave.male;
                    g.gameObject.GetComponent<MainPlayerDataInput>().currentSkin = MainSave.skin;
                }
            }*/
        }


        loadEverySphereEx();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public static string LoadJsonFromLocation(string path)
    {
        //string jsonFilePath = path.Replace(".json", "");
        string jsonFilePath = path/*.Replace(".json", "")*/;
        Debug.Log("Reading Text File: " + jsonFilePath);
        string loadedJsonFile = "";

        if (File.Exists(jsonFilePath))
        {
            loadedJsonFile = File.ReadAllText(jsonFilePath);
            Debug.Log("\nReading Text File: " + jsonFilePath);
            Debug.Log("\nReading Text File: " + loadedJsonFile);

        }
        else
        {
            Debug.Log("file does not exists");
        }
        return loadedJsonFile;
        Debug.Log("Reading Text File: " + jsonFilePath);
    }

    public void save()
    {
        bool testMainModel=false;
        GameObject mainModel = this.gameObject;
        /*
        foreach(GameObject g in models)
        {
            if (g.gameObject.GetComponent<MainPlayerDataInput>()!=null &&!g.gameObject.GetComponent<MainPlayerDataInput>().model)
            {
                mainModel = g;
                testMainModel = true;
            }
        }
        if (testMainModel)
        {
            MainSave.skin = mainModel.gameObject.GetComponent<MainPlayerDataInput>().currentSkin;
            MainSave.male = mainModel.gameObject.GetComponent<MainPlayerDataInput>().male;
            string json = JsonUtility.ToJson(MainSave);
            Debug.Log(json);
            File.WriteAllText(Application.dataPath + "/MainSave" + "/" + "Save" + ".json", json);
        }
        */
    }

    public void saveSphereEx(List<GameObject> sphereListStorage)
    {
        DirectoryInfo sphExDir = new DirectoryInfo(Application.streamingAssetsPath + "/exSphere");
        print("Streaming Assets Path: " + (Application.streamingAssetsPath + "/exSphere"));
        FileInfo[] allFiles = sphExDir.GetFiles("*.*");

        string fileName="";
        int i = 0;
        while (fileName == "")
        {
            if(allFiles[i].Name == "ex" + i + ".json")
            {
                i++;
            }
            else
            {
                fileName = "ex" + i;
            }
        }

        string path = (Application.streamingAssetsPath + "/exSphere" + "/" + fileName + ".json");
        SphereExSave newSave = new SphereExSave();
        newSave.ex = new List<SphereSave>();

        foreach (GameObject g in sphereListStorage)
        {
            SphereSave SpS = new SphereSave();
            SpS.position = g.gameObject.transform.position;
            SpS.order = g.gameObject.GetComponent<SphereControl>().order;
            SpS.left = g.gameObject.GetComponent<SphereControl>().left;
            SpS.right = g.gameObject.GetComponent<SphereControl>().right;
            newSave.ex.Add(SpS);
        }

        string json = JsonUtility.ToJson(newSave);
        Debug.Log(json);
        Debug.Log(path);
        File.WriteAllText(path, json);
    }

    public void loadEverySphereEx()
    {
        sphereSaves.Clear();
        DirectoryInfo sphExDir = new DirectoryInfo(Application.streamingAssetsPath + "/exSphere");
        print("Streaming Assets Path: " + (Application.streamingAssetsPath + "/exSphere"));
        FileInfo[] allFiles = sphExDir.GetFiles("*.*");

        //int i = 0;

        foreach (FileInfo f in allFiles)
        {
            Debug.Log(f.Name);
            if (f.Name.Contains(".json")&&!f.Name.Contains("meta"))
            {
                Debug.Log("Reading "+f.Name);
                SphereExSave newLoad = new SphereExSave();
                newLoad = JsonUtility.FromJson<SphereExSave>(LoadJsonFromLocation(Application.streamingAssetsPath + "/exSphere/" + f.Name));
                sphereSaves.Add(newLoad);
            }
        }
    }

    public List<GameObject> loadSpecificSphereEx(int i, Vector3 storage)
    {
        List<GameObject> sphereListStorage = new List<GameObject>();
        string fileName = "ex" + i;
        string path = (Application.streamingAssetsPath + "/exSphere" + "/" + fileName + ".json");
        SphereExSave newLoad = new SphereExSave();
        newLoad = JsonUtility.FromJson<SphereExSave>(LoadJsonFromLocation(path));

        foreach(SphereSave SpS in newLoad.ex)
        {

        }

        return sphereListStorage;
    }

    public void saveModelEx()
    {

    }

    public void loadModelEx()
    {

    }

}
