﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeNode : Node<int>
{
    public TreeNode tP;
    public List<TreeNode> tC = new List<TreeNode>();
    public string varName;
    public bool b_Result;
    public string s_Result;
}
