﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour
{
    public Vector3 inActionPosition;
    public Vector3 storagePosition;
    public Vector3 movPosition;
    public List<GameObject> MenuObjects = new List<GameObject>();
    public List<GameObject> currentButton = new List<GameObject>();

    public GameObject currentMenu;
    public GameObject mainMenu;
    public GameObject movMenu;

    public GameObject sphereButton;
    public GameObject modelButton;

    public GameObject systemManager;

    public List<GameObject> props = new List<GameObject>();
    public List<Vector3> propsPosition = new List<Vector3>();
    public List<Vector3> propsRotation = new List<Vector3>();

    public Vector3 propsStorage;

    public int raiseNumber = 0;
    public int optionNumber = 0;
    public float buttonOffset = 1.15f;

    // Start is called before the first frame update
    void Start()
    {


        for (int i = 0; i < mainMenu.transform.childCount; i++)
        {
            MenuObjects.Add(mainMenu.transform.GetChild(i).gameObject);
        }
        menuChange();

        foreach (GameObject g in MenuObjects)
        {
            if (g == currentMenu)      
            {
                g.transform.position = inActionPosition; 
            }
            else if(g == movMenu)
            {
                g.transform.position = movPosition;
            }
            else
            {
                g.transform.position = storagePosition;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void propActivation(int num)
    {
        props[num].gameObject.transform.position = propsPosition[num];
        props[num].gameObject.transform.eulerAngles = propsRotation[num];
    }

    public void propDeactivation(int num)
    {
        props[num].gameObject.transform.position = propsStorage;
    }

    public void hideMenu()
    {
        currentMenu.transform.position = storagePosition;
        movMenu.transform.position = storagePosition;
    }

    public void showMenu()
    {
        currentMenu.transform.position = inActionPosition;
        movMenu.transform.position = movPosition;
    }

    void menuChange()
    {
        while (currentButton.Count > 0)
        {
            currentButton.RemoveAt(0);
            optionNumber = 0;
            raiseNumber = 0;
        }
        
        for (int i = 0; i < currentMenu.transform.childCount; i++)
        {
            currentButton.Add(currentMenu.transform.GetChild(i).gameObject);
        }

        foreach (GameObject g in currentButton)
        {
            optionNumber++;
        }
    }

    public void updateCurrentPosition(GameObject newCurrent)
    {
        
        bool hasInMenu = false;
        foreach (GameObject g in MenuObjects)
        {
            if (newCurrent != g)
            {
            }
            else
            {
                hasInMenu = true;
            }
        }
        if (hasInMenu)
        {
            currentMenu.transform.position = storagePosition;
            currentMenu = newCurrent;
            currentMenu.transform.position = inActionPosition;

            if(newCurrent.gameObject.name == "MenuConfig")
            {
                propActivation(0);
                propActivation(1);

            }
            else
            {
                foreach(GameObject g in props)
                {
                    g.gameObject.transform.position = propsStorage;
                }
            }

            menuChange();
        }
    }
}
