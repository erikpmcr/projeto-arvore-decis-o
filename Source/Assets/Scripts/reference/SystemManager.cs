﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class SystemManager : MonoBehaviour
{
    public GameObject menu;

    // Start is called before the first frame update
    void Start()
    {
        #region
        //insere botões novos apartir do número de exercicios
        //float buttonPosition;
        int exN = 0;
        foreach (SphereExSave sEs in this.gameObject.GetComponent<AutoSave>().sphereSaves)
        {
            foreach(Transform eachChild in menu.transform)
            {
                if (eachChild.name == "MenuExEsf")
                {
                    GameObject g = Instantiate(menu.gameObject.GetComponent<MenuControl>().sphereButton, eachChild);
                    g.transform.localPosition = new Vector3(0, 1.15f - (eachChild.childCount - 1)* 1.15f, 0);
                    //g.gameObject.GetComponent<GazeButtonControl>().functionInt = exN;
                    g.transform.GetChild(0).GetComponent<TextMeshPro>().text = (exN + 1).ToString();
                    g.gameObject.name = "ButtonSphereN" + exN;
                    exN++;
                    //eachChild.childCount
                }
            }
           
        }
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startEx()
    {
        menu.gameObject.GetComponent<MenuControl>().hideMenu();

    }

    public void endEx()
    {
        menu.gameObject.GetComponent<MenuControl>().showMenu();
    }

    public SphereExSave callExSphere(int i)
    {
        return this.gameObject.GetComponent<AutoSave>().sphereSaves[i];
    }

    /*
     
        
        */
}
