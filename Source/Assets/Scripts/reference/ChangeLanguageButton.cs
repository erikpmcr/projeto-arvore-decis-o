﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLanguageButton : MonoBehaviour
{
    public GameObject textManager;
    public string language;
    // Start is called before the first frame update
    void Start()
    {
        textManager = GameObject.Find("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        if(textManager == null)
        {
            textManager = GameObject.Find("GameManager");
        }
    }
    public void changeLanguage()
    {
        textManager.GetComponent<TextManager>().languageSelect = language;
    }
}
