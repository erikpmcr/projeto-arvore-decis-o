﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereExControl : MonoBehaviour
{
    public List<GameObject> sphereListStorage = new List<GameObject>();

    public List<GameObject> sphereListInUse = new List<GameObject>();

    public Vector3 storagePosition;

    public float exTime;

    public float exMaxTime;

    public int stage;

    public bool pause;

    public bool save;

    public bool start;

    public bool end;

    // Start is called before the first frame update
    void Start()
    {
        exTime = exMaxTime;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!pause && start)
        {
            exTime -= Time.deltaTime;
            //GameObject g in sphereListStorage
            for (int i =0; i<sphereListStorage.Count;i++)
            {
                GameObject g = sphereListStorage[i];
                if (g.gameObject == null)
                {
                    sphereListStorage.Remove(g);
                }
                if (g.gameObject.GetComponent<SphereControl>().order == stage)
                {
                    sphereListInUse.Add(g);
                    sphereListStorage.Remove(g);
                    g.transform.position = g.gameObject.GetComponent<SphereControl>().plannedPosition;
                }
            }
            for (int i = 0; i < sphereListInUse.Count; i++)
            {
                GameObject g = sphereListInUse[i];
                if (g.gameObject == null)
                {
                    sphereListInUse.Remove(g);
                }
            }

            if (sphereListInUse.Count == 0)
            {
                stage++;
            }

            if ((sphereListStorage.Count == 0 && sphereListInUse.Count == 0) || exTime <= 0)
            {

                for (int i = 0; i < sphereListStorage.Count; i++)
                {
                    GameObject g = sphereListStorage[i];
                    sphereListStorage.Remove(g);
                    Destroy(g.gameObject);
                }
                for (int i = 0; i < sphereListInUse.Count; i++)
                {
                    GameObject g = sphereListInUse[i];
                    sphereListStorage.Remove(g);
                    Destroy(g.gameObject);
                }

                start = false;
                end = true;
                //Destroy(this.gameObject);
            }
        }
        if (save && pause)
        {
            if(this.gameObject.GetComponent<AutoSave>() != null)
            {
                this.gameObject.GetComponent<AutoSave>().saveSphereEx(sphereListStorage);
            }
            save = false;
        }

        if (end)
        {
            exTime = exMaxTime;
            end = false;
            start = false;
            stage = 0;
            this.gameObject.GetComponent<SystemManager>().menu.gameObject.GetComponent<MenuControl>().showMenu();
        }
    }

    public void getExercise(int i)
    {
        SphereExSave tempExS = this.gameObject.GetComponent<SystemManager>().callExSphere(i);
        for(int k = 0; k < tempExS.ex.Count; k++){
            SphereSave sPs = tempExS.ex[k];
            GameObject g = Instantiate(this.gameObject.GetComponent<AutoSave>().prefab_sphere, storagePosition, Quaternion.identity,null) as GameObject;
            g.GetComponent<SphereControl>().plannedPosition = sPs.position;
            g.GetComponent<SphereControl>().left = sPs.left;
            g.GetComponent<SphereControl>().right = sPs.right;
            g.GetComponent<SphereControl>().order = sPs.order;
            sphereListStorage.Add(g);
        }

    }
}
