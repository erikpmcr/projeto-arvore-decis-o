﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class LineReader : MonoBehaviour
{
    public string file;
    public List<string> gameText;
    public FileInfo theSourceFile;
    void Start()
    {
        theSourceFile = new FileInfo(Application.dataPath + "/Resources/" + file);
        StreamReader reader = theSourceFile.OpenText();

        string text;

        do
        {
            text = reader.ReadLine();
            gameText.Add(text);
            //Console.WriteLine(text);
            print(text);
        } while (text != null);
    }
}
