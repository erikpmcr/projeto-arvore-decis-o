﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticText : MonoBehaviour
{
    public GameObject TextManager;
    public string currentText;
    public string textSelect;
    public string language;
    public bool early;
    // Start is called before the first frame update
    void Start()
    {
        if (TextManager == null)
        {
            TextManager = GameObject.Find("GameManager");
        }
        currentText = textSelect;
        this.GetComponent<Text>().text = TextManager.GetComponent<TextManager>().staticTextResponce(currentText);
    }

    // Update is called once per frame
    void Update()
    {
        if (TextManager == null)
        {
            TextManager = GameObject.Find("GameManager");
        }
        else
        {
            if (currentText != textSelect || this.GetComponent<Text>().text == "" || language!= TextManager.GetComponent<TextManager>().currentLanguage)
            {
                language = TextManager.GetComponent<TextManager>().currentLanguage;
                currentText = textSelect;
                if (!early)
                {
                    this.GetComponent<Text>().text = TextManager.GetComponent<TextManager>().staticTextResponce(currentText);
                }
                else
                {
                    this.GetComponent<Text>().text = TextManager.GetComponent<TextManager>().earlyStaticTextResponce(currentText);
                }
            }
        }
    }

    public void sumText(string txt)
    {
        this.GetComponent<Text>().text += txt;
    }
    public void sumResetText(string txt)
    {
        this.GetComponent<Text>().text = TextManager.GetComponent<TextManager>().staticTextResponce(currentText) + txt;
    }
}
