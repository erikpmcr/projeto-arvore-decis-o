﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereHandCollider : MonoBehaviour
{
    public bool leftHand;
    public bool rightHand;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BlueSphere") && leftHand)
        {
            if (other.gameObject.GetComponent<SphereControl>() != null
            && !other.gameObject.GetComponent<SphereControl>().connect)
            {
                Destroy(other.gameObject);
            }

            else if (other.gameObject.GetComponent<SphereControl>() != null
            && other.gameObject.GetComponent<SphereControl>().connect)
            {
                other.gameObject.transform.parent = this.gameObject.transform;
            }
        }
        if (other.gameObject.CompareTag("RedSphere") && rightHand)
        {
            if (other.gameObject.GetComponent<SphereControl>() != null
            && !other.gameObject.GetComponent<SphereControl>().connect)
            {
                Destroy(other.gameObject);
            }
            else if (other.gameObject.GetComponent<SphereControl>() != null
            && other.gameObject.GetComponent<SphereControl>().connect)
            {
                other.gameObject.transform.parent = this.gameObject.transform;
            }
        }
    }

}
