﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereControl : MonoBehaviour
{
    public bool connect;
    public Material red;
    public Material blue;
    public bool left;
    public bool right;

    public Vector3 plannedPosition;

    public int order;

    public bool destroySphere = false;

    // Start is called before the first frame update
    void Start()
    {
        if (left)
        {
            this.gameObject.GetComponent<MeshRenderer>().material = blue;
            this.gameObject.tag = "BlueSphere";
        }
        else if (right)
        {
            this.gameObject.GetComponent<MeshRenderer>().material = red;
            this.gameObject.tag = "RedSphere";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!connect)
        {
            this.transform.parent = null;
        }

        if (destroySphere)
        {
            Destroy(this.gameObject);
        }
    }
}
