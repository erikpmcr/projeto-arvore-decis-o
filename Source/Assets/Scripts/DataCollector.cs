﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct DataTable
{
    [SerializeField]
    public ListWrapper<List<float>> table;
    [SerializeField]
    public List<string> vars;
    [SerializeField]
    public List<string> result;
}

[System.Serializable]
public class ListWrapper<T>
{
    [SerializeField]
    public List<T> myList = new List<T>();

    public void Add(T var)
    {
        myList.Add(var);
    }
    public T get(int c)
    {
        return myList[c];
    }
    public T RemoveAt(int c)
    {
        T temp = myList[c];
        myList.RemoveAt(c);
        return temp;
    }

}

public class DataCollector : MonoBehaviour
{
    public bool buildList;

    [SerializeField]
    public DataTable main = new DataTable();
    
    // Start is called before the first frame update
    void Start()
    {
        
        main.table = new ListWrapper<List<float>>();
        main.vars = new List<string>();
        main.result = new List<string>();
        main.table.Add(new List<float>());
        main.table.get(0).Add(1f);
        main.table.get(0).Add(1f);
        main.table.get(0).Add(1f);
        main.table.Add(new List<float>());
        main.table.get(1).Add(2f);
        main.table.get(1).Add(2f);
        main.table.get(1).Add(2f);
        main.vars.Add("var1");
        main.vars.Add("var2");
        main.result.Add("true");
        main.result.Add("false");
        if (buildList)
        {
            this.gameObject.GetComponent<DataReaderAndWriter>().saveTable(main);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
