﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataReaderAndWriter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void saveTable(DataTable d)
    {
        DirectoryInfo tabDir;
        if (Directory.Exists(Application.streamingAssetsPath + ""))
        {
            tabDir = new DirectoryInfo(Application.streamingAssetsPath + "");
        }
        else
        {
            Directory.CreateDirectory(Application.streamingAssetsPath + "");
            tabDir = new DirectoryInfo(Application.streamingAssetsPath + "");
        }
        print("Streaming Assets Path: " + (Application.streamingAssetsPath + ""));

        FileInfo[] allFiles = tabDir.GetFiles("*.*");

        string fileName = "";
        int i = 0;
        fileName = "test";

        string path = (Application.streamingAssetsPath + "/" + fileName + ".json");
        DataTable newSave = new DataTable();
        newSave = d;

        string json = JsonUtility.ToJson(newSave);

        Debug.Log(json);
        Debug.Log(path);
        File.WriteAllText(path, json);
    }
    public DataTable loadTable()
    {
        return new DataTable();
    }
    public static string LoadJsonFromLocation(string path)
    {
        //string jsonFilePath = path.Replace(".json", "");
        string jsonFilePath = path/*.Replace(".json", "")*/;
        Debug.Log("Reading Text File: " + jsonFilePath);
        string loadedJsonFile = "";

        if (File.Exists(jsonFilePath))
        {
            loadedJsonFile = File.ReadAllText(jsonFilePath);
            Debug.Log("\nReading Text File: " + jsonFilePath);
            Debug.Log("\nReading Text File: " + loadedJsonFile);

        }
        else
        {
            Debug.Log("file does not exists");
        }
        return loadedJsonFile;
        Debug.Log("Reading Text File: " + jsonFilePath);
    }

}
